import React from 'react';
import TextStyled from './TextStyled';

function Text(props) {
    const {styleText} = props;
    console.log(props);
    return (
        <TextStyled color={styleText.color}>Test</TextStyled>
    )
}

export default Text;