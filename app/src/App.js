import React from 'react';
import logo from './logo.svg';
import './App.css';
import Text from './Text';
import Text2 from './Text2';
import mock from './fixtures';


function App() {
    return (
    <div className="App">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
        To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <Text styleText={mock[0]} />
        <Text2 styleText={mock[0]}>xxx</Text2>
    </div>
    )
}

export default App;
