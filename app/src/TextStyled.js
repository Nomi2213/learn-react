import styled from 'react-emotion';

const TextStyled = styled('div')( props => ({
    background: 'green',
    color: props.color
}))

export default TextStyled;